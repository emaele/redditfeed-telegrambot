module gitlab.com/emaele/redditfeed-telegrambot

go 1.12

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible
	github.com/golang/protobuf v1.4.2 // indirect
	github.com/mitchellh/mapstructure v1.3.0 // indirect
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
	github.com/turnage/graw v0.0.0-20200404033202-65715eea1cd0
	golang.org/x/net v0.0.0-20200513185701-a91f0712d120 // indirect
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d // indirect
	google.golang.org/appengine v1.6.6 // indirect
)
