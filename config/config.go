package config

import (
	"errors"

	"github.com/BurntSushi/toml"
)

// Config is the bot configuration representation, read
// from a configuration file.
type Config struct {
	//Telegram stuff
	TelegramTokenBot string
	ChatID           int64

	//List of subreddits
	Sources []string

	//Reddit Application fields
	Agent  string
	ID     string
	Secret string

	//Reddit username and Passwords
	Username string
	Password string
}

// ReadConfig loads the values from the config file
func ReadConfig(path string) (Config, error) {
	var conf Config

	if _, err := toml.DecodeFile(path, &conf); err != nil {
		return Config{}, err
	}

	if conf.TelegramTokenBot == "" {
		return newErr("missing Bot token")
	} else if conf.ChatID == 0 {
		return newErr("missing ID")
	} else if len(conf.Sources) == 0 {
		return newErr("missing reddit sources")
	} else if conf.Agent == "" {
		return newErr("missing useragent value")
	} else if conf.ID == "" {
		return newErr("missing appid value")
	} else if conf.Secret == "" {
		return newErr("missing app secret value")
	} else if conf.Username == "" {
		return newErr("missing reddit username")
	} else if conf.Password == "" {
		return newErr("missing reddit password")
	}

	return conf, nil
}

func newErr(message string) (Config, error) {
	return Config{}, errors.New(message)
}
