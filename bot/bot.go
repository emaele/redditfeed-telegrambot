package bot

import (
	"fmt"
	"strings"

	"github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/turnage/graw/reddit"
	"gitlab.com/emaele/redditfeed-telegrambot/utility"

	conf "gitlab.com/emaele/redditfeed-telegrambot/config"
)

// PostingBot defines postbot structure
type PostingBot struct {
	RBot   reddit.Bot
	TBot   *tgbotapi.BotAPI
	Config conf.Config
}

// Post manages incoming reddit post
func (r *PostingBot) Post(p *reddit.Post) error {
	switch {
	case p.NSFW:
		// We hide NSFW content
		msg := tgbotapi.NewMessage(r.Config.ChatID, fmt.Sprintf("Uh oh, nsfw content! 🔞\n%s", p.URL))
		msg.DisableWebPagePreview = true
		msg.ReplyMarkup = utility.SetupInlineKeyboard(p.Subreddit, p.Permalink)
		r.TBot.Send(msg)
	case p.Media.RedditVideo.IsGIF:
		msg := tgbotapi.NewDocumentUpload(r.Config.ChatID, p.URL)
		msg.ReplyMarkup = utility.SetupInlineKeyboard(p.Subreddit, p.Permalink)
		r.TBot.Send(msg)
	case strings.Contains(p.URL, ".jpg") || strings.Contains(p.URL, ".png"):
		msg := tgbotapi.NewPhotoUpload(r.Config.ChatID, "")
		msg.FileID = p.URL
		msg.UseExisting = true
		msg.ReplyMarkup = utility.SetupInlineKeyboard(p.Subreddit, p.Permalink)
		r.TBot.Send(msg)
	default:
		msg := tgbotapi.NewMessage(r.Config.ChatID, p.URL)
		msg.ReplyMarkup = utility.SetupInlineKeyboard(p.Subreddit, p.Permalink)
		r.TBot.Send(msg)
	}
	return nil
}
